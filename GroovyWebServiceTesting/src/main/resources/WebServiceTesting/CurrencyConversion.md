 Convert Currency From NZD to INR

 
 Given I have the SOAP WSDL for Conversion rates Then I run the soap call to convert the currency between different countries

### [Example](- "currencyconverter")

When I make a Soap Call to convert from [NZD](- "#nzd") to [INR](- "#inr") to the Currency [converter](- "#result = convert(#nzd,#inr)") 
then I get the currency rate as [49.377](- "?=#result.response") 