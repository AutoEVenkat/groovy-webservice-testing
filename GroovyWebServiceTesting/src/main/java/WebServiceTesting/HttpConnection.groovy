package WebServiceTesting

/**
 * Created by venkita on 1/25/2017.
 */

import wslite.soap.SOAPClient
def url = 'http://www.webservicex.net/CurrencyConvertor.asmx?WSDL'
def client = new SOAPClient(url)
def action = "http://www.webserviceX.NET/ConversionRate"
def response = client.send(SOAPAction: action) {
    body {
        ConversionRate(xmlns: 'http://www.webserviceX.NET/') {
            FromCurrency('NZD')
            ToCurrency('INR')
        }
    }
}
assert response.httpResponse.statusCode == 200
println response.ConversionRateResponse.ConversionRateResult