package WebServiceTesting

import org.concordion.integration.junit4.ConcordionRunner
import org.junit.runner.RunWith
import wslite.soap.SOAPClient

/**
 * Created by venkita on 1/25/2017.
 */
@RunWith(ConcordionRunner.class)
public class CurrencyConversionFixture {

    //Function should always be in lower case for the specification to understand in stupid Concordian!!
    public Result convert(String nzd,String inr) {
     def url = 'http://www.webservicex.net/CurrencyConvertor.asmx?WSDL'
     def client = new SOAPClient(url)

        Result result = new Result();
      def action = "http://www.webserviceX.NET/ConversionRate";
         def response =client.send(SOAPAction: action) {
            body {
                ConversionRate(xmlns: 'http://www.webserviceX.NET/') {
                    FromCurrency(nzd)
                    ToCurrency(inr)
                }
            }
        }

         result.response = response.ConversionRateResponse.ConversionRateResult
         return  result;
    }

    class Result {
        public String response;
    }

}
